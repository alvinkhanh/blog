<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <h1>Buat Akun Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/post" method="post">
    @csrf
    <label for="fname">First Name:</label>
    <br>
    <input type="text" name="fname" placeholder="Masukan nama depan anda" id="fname">
    <br><br>
    <label for="lname">Last Name:</label>
    <br>
    <input type="text" name="lname" placeholder="Masukan nama belakang anda" id="lname">
    <br><br>
    <label for="gender">Gender:</label>
    <br>
    <input type="radio" id="man" name="gender" value="man">
    <label for="male">Man</label><br>
    <input type="radio" id="women" name="gender" value="women">
    <label for="female">Women</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>
    <br>
    <label for="national">Nationality</label>
    <select id="national">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="other">Other</option>
    </select>
    <br><br>
    <label for="">Language Spoken:</label>
        <br>
            <input type="checkbox" id="vehicle1" name="language1" value="indo">
            <label for="vehicle1"> Bahasa Indonesia</label><br>
            <input type="checkbox" id="vehicle2" name="language1" value="eng">
            <label for="vehicle2"> Inggris</label><br>
            <input type="checkbox" id="vehicle3" name="language1" value="other">
            <label for="vehicle3"> Other</label>
        <br><br>
        <label for="bio">BIO :</label>
        <br>
        <textarea name="" id="bio" cols="30" rows="10"></textarea>
        <br>
    <button value="kirim" type="submit">sign up</button>
    </form>
</body>
</html>