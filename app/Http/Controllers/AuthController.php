<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form_sign_up(){
        return view('data.form_sign_up');
    }
    public function post(Request $request){
        $fname = $request->fname;
        $lname = $request->lname;

        return view('data.welcome', compact('fname','lname'));
    }
}

